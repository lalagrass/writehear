
var lastTabId = -1;

function createMyTab()
{
	chrome.tabs.create(
		{
			'url': chrome.extension.getURL('speechdemo.html'),
			'active': true
		},
		function(tab)
		{
			lastTabId = tab.id;
		}
	);
}

function activeMyTab()
{
	chrome.tabs.update(
		lastTabId,
		{
			"highlighted":true
		}
	);
}

function initBackground() {
	chrome.browserAction.onClicked.addListener(
		function(tab)
		{
			if (lastTabId > 0)
			{
				chrome.tabs.get(
					lastTabId,
					function()
					{
						if (chrome.runtime.lastError) {					
							createMyTab();							
						} else {
							activeMyTab();
						}
					}
				);
			}
			else
			{
				createMyTab();
			}
		}
	);
}

initBackground();
